﻿using System.Security.Cryptography;
using System.Text;

namespace UserService.Impl
{
    internal class HashCalculator : IHashCalculator
    {
        public string Calculate(string source)
        {
            var hashAlg = SHA256.Create();
            var hashBytes = hashAlg.ComputeHash(Encoding.UTF8.GetBytes(source));
            var sb = new StringBuilder(hashBytes.Length * 2);
            for (var i = 0; i < hashBytes.Length; i++)
            {
                sb.Append(hashBytes[i].ToString("X2"));
            }
            return sb.ToString();
        }
    }
}