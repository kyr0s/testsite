﻿namespace UserService.Impl
{
    internal interface IHashCalculator
    {
        string Calculate(string source);
    }
}