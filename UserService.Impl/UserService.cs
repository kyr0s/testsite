﻿using System;
using System.Threading.Tasks;
using UserService.DAL;
using UserService.Models;

namespace UserService.Impl
{
    internal class UserService : IUserService
    {
        private readonly IUserRepository _repository;
        private readonly IHashCalculator _hashCalculator;

        public UserService(
            IUserRepository repository,
            IHashCalculator hashCalculator)
        {
            _repository = repository;
            _hashCalculator = hashCalculator;
        }

        public Task<User> AuthenticateAsync(string login, string password)
        {
            var hash = _hashCalculator.Calculate(password);
            return _repository.AuthenticateAsync(login, hash);
        }

        public Task<User> GetAsync(Guid userId)
        {
            return _repository.GetAsync(userId);
        }

        public async Task<User> RegisterAsync(RegistrationRequest request)
        {
            var user = new User
            {
                Id = Guid.NewGuid(),
                CreateDate = DateTime.Now,
                Login = request.Login,
                Password = _hashCalculator.Calculate(request.Password),
                Email = request.Email,
                Name = request.Name
            };
            await _repository.SaveAsync(user).ConfigureAwait(false);
            return user;
        }
    }
}
