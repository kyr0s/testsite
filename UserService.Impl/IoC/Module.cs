﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using UserService.DAL.IoC;

namespace UserService.Impl.IoC
{
    public static class Module
    {
        public static IServiceCollection WithUserService(this IServiceCollection @this, DbContextOptions options)
        {
            return @this
                .WithUsersDataLayer(options)
                .AddScoped<IUserService, UserService>()
                .AddScoped<IHashCalculator, HashCalculator>();
        }
    }
}
