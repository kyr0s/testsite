﻿using System;
using System.Threading.Tasks;
using UserService.Models;

namespace UserService
{
    public interface IUserService
    {
        Task<User> AuthenticateAsync(string login, string password);
        Task<User> RegisterAsync(RegistrationRequest request);
        Task<User> GetAsync(Guid userId);
    }
}
