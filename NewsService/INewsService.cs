﻿using System;
using System.Threading.Tasks;
using NewsService.Models;

namespace NewsService
{
    public interface INewsService
    {
        Task<Guid> CreateAsync(CreateNewsRequest request);
        Task<NewsPreview[]> GetPageAsync(int pageNumber, int pageSize);
        Task<News> GetAsync(Guid newsId);
        Task<Comment[]> SelectCommentsAsync(Guid newsId);
        Task<Guid> CreateCommentAsync(Guid newsId, Guid userId, string text);
    }
}
