﻿using System;

namespace NewsService.Models
{
    public class NewsPreview
    {
        public Guid Id { get; set; }

        public DateTime Date { get; set; }

        public string Title { get; set; }
    }
}