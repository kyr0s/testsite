﻿using System;

namespace NewsService.Models
{
    public class Comment
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string Text { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
