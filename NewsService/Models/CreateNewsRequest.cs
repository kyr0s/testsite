﻿namespace NewsService.Models
{
    public class CreateNewsRequest
    {
        public string Title { get; set; }

        public string Text { get; set; }
    }
}