﻿using System;

namespace NewsService.Models
{
    public class News
    {
        public Guid Id { get; set; }

        public DateTime Date { get; set; }

        public string Title { get; set; }

        public string Text { get; set; }
    }
}
