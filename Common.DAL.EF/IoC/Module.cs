﻿using System;
using Common.DAL.IoC;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Common.DAL.EF.IoC
{
    public static class Module
    {
        public static IServiceCollection WithEfDbContext<TZone>(this IServiceCollection @this, Func<DbContext> dbContextFactory) where TZone: IZoneMarker
        {
            return @this
                .WithDataLayer<TZone>()
                .AddScoped<IDataContext<TZone>, EntityFrameworkDataContext<TZone>>(p => CreateDataContext<TZone>(p, dbContextFactory));
        }

        private static EntityFrameworkDataContext<TZone> CreateDataContext<TZone>(IServiceProvider serviceProvider, Func<DbContext> dbContextFactory) where TZone: IZoneMarker
        {
            var dbContext = dbContextFactory();
            var logger = serviceProvider.GetRequiredService<ILogger>();
            return new EntityFrameworkDataContext<TZone>(dbContext, logger);
        }
    }
}
