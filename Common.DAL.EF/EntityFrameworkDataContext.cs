﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.DAL.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Logging;

namespace Common.DAL.EF
{
    internal class EntityFrameworkDataContext<TZone> : IDataContext<TZone> where TZone: IZoneMarker
    {
        private readonly ILogger _logger;
        private readonly DbContext _dataContext;

        public EntityFrameworkDataContext(
           DbContext dbContext,
           ILogger logger)
        {
            _logger = logger;
            _dataContext = dbContext;
        }

        public void Dispose()
        {
            _dataContext.Dispose();
        }

        public IQueryable<T> Query<T>() where T : class
        {
            return _dataContext.Set<T>();
        }

        public void Create(Type type, object item)
        {
            SafeCall(() =>
            {
                AssertIsClass(type);
                _dataContext.Add(item);
                AssertAtMostOneChanged();
                _dataContext.SaveChanges();
            });
        }

        public Task CreateAsync(Type type, object item)
        {
            return SafeCallAsync(() =>
            {
                AssertIsClass(type);
                _dataContext.Add(item);
                return _dataContext.SaveChangesAsync();
            });
        }

        public void Create<T>(T item) where T : class
        {
            SafeCall(() =>
            {
                _dataContext.Add(item);
                AssertAtMostOneChanged();
                _dataContext.SaveChanges();
            });
        }

        public Task CreateAsync<T>(T item) where T : class
        {
            return SafeCallAsync(() =>
            {
                _dataContext.Add(item);
                AssertAtMostOneChanged();
                return _dataContext.SaveChangesAsync();
            });
        }

        public void CreateAll(Type type, object[] items)
        {
            SafeCall(() =>
            {
                AssertIsClass(type);
                _dataContext.AddRange(items);
                AssertExactlyChanged(items);
                _dataContext.SaveChanges();
            });
        }

        public Task CreateAllAsync(Type type, object[] items)
        {
            return SafeCallAsync(() =>
            {
                AssertIsClass(type);
                _dataContext.AddRange(items);
                AssertExactlyChanged(items);
                return _dataContext.SaveChangesAsync();
            });
        }

        public void CreateAll<T>(T[] items) where T : class
        {
            SafeCall(() =>
            {
                _dataContext.Set<T>().AddRange(items);
                AssertExactlyChanged(items);
                _dataContext.SaveChanges();
            });
        }

        public Task CreateAllAsync<T>(T[] items) where T : class
        {
            return SafeCallAsync(() =>
            {
                _dataContext.Set<T>().AddRange(items);
                AssertExactlyChanged(items);
                return _dataContext.SaveChangesAsync();
            });
        }

        public void Update(Type type, object item)
        {
            SafeCall(() =>
            {
                AssertIsClass(type);
                AssertAttached(item);
                AssertAtMostOneChanged();
                _dataContext.SaveChanges();
            });
        }

        public Task UpdateAsync(Type type, object item)
        {
            return SafeCallAsync(() =>
            {
                AssertIsClass(type);
                AssertAttached(item);
                AssertAtMostOneChanged();
                return _dataContext.SaveChangesAsync();
            });
        }

        public void Update<T>(T item) where T : class
        {
            SafeCall(() =>
            {
                AssertAttached(item);
                AssertAtMostOneChanged();
                _dataContext.SaveChanges();
            });
        }

        public Task UpdateAsync<T>(T item) where T : class
        {
            return SafeCallAsync(() =>
            {
                AssertAttached(item);
                AssertAtMostOneChanged();
                return _dataContext.SaveChangesAsync();
            });
        }

        public void UpdateAll(Type type, object[] items)
        {
            SafeCall(() =>
            {
                AssertIsClass(type);
                AssertAttached(items);
                AssertExactlyChanged(items);
                _dataContext.SaveChanges();
            });
        }

        public Task UpdateAllAsync(Type type, object[] items)
        {
            return SafeCallAsync(() =>
            {
                AssertIsClass(type);
                AssertAttached(items);
                AssertExactlyChanged(items);
                return _dataContext.SaveChangesAsync();
            });
        }

        public void UpdateAll<T>(T[] items) where T : class
        {
            SafeCall(() =>
            {
                AssertAttached(items);
                AssertExactlyChanged(items);
                _dataContext.SaveChanges();
            });
        }

        public Task UpdateAllAsync<T>(T[] items) where T : class
        {
            return SafeCallAsync(() =>
            {
                AssertAttached(items);
                AssertExactlyChanged(items);
                return _dataContext.SaveChangesAsync();
            });
        }

        public void Delete(Type type, object item)
        {
            SafeCall(() =>
            {
                AssertIsClass(type);
                AssertAttached(item);
                _dataContext.Remove(item);
                AssertAtMostOneChanged();
                _dataContext.SaveChanges();
            });
        }

        public Task DeleteAsync(Type type, object item)
        {
            return SafeCallAsync(() =>
            {
                AssertIsClass(type);
                AssertAttached(item);
                _dataContext.Remove(item);
                AssertAtMostOneChanged();
                return _dataContext.SaveChangesAsync();
            });
        }

        public void Delete<T>(T data) where T : class
        {
            SafeCall(() =>
            {
                AssertAttached(data);
                _dataContext.Remove(data);
                AssertAtMostOneChanged();
                _dataContext.SaveChanges();
            });
        }

        public Task DeleteAsync<T>(T data) where T : class
        {
            return SafeCallAsync(() =>
            {
                AssertAttached(data);
                _dataContext.Remove(data);
                AssertAtMostOneChanged();
                return _dataContext.SaveChangesAsync();
            });
        }

        public void DeleteAll(Type type, object[] items)
        {
            SafeCall(() =>
            {
                AssertIsClass(type);
                AssertAttached(items);
                _dataContext.RemoveRange(items);
                AssertExactlyChanged(items);
                _dataContext.SaveChanges();
            });
        }

        public Task DeleteAllAsync(Type type, object[] items)
        {
            return SafeCallAsync(() =>
            {
                AssertIsClass(type);
                AssertAttached(items);
                _dataContext.RemoveRange(items);
                AssertExactlyChanged(items);
                return _dataContext.SaveChangesAsync();
            });
        }

        public void DeleteAll<T>(T[] items) where T : class
        {
            SafeCall(() =>
            {
                AssertAttached(items);
                _dataContext.Set<T>().RemoveRange(items);
                AssertExactlyChanged(items);
                _dataContext.SaveChanges();
            });
        }

        public Task DeleteAllAsync<T>(T[] items) where T : class
        {
            return SafeCallAsync(() =>
            {
                AssertAttached(items);
                _dataContext.Set<T>().RemoveRange(items);
                AssertExactlyChanged(items);
                return _dataContext.SaveChangesAsync();
            });
        }

        private void AssertAtMostOneChanged()
        {
            if (GetChangeItems().Take(2).Count() > 1)
            {
                throw new UnexpectedChangesException();
            }
        }

        private void AssertExactlyChanged(object[] items)
        {
            var changedItemsCount = items
                .Distinct()
                .Select(i => _dataContext.Entry(i))
                .Where(e => e.State != EntityState.Detached && e.State != EntityState.Unchanged)
                .Count();
            var contextChangedItemsCount = GetChangeItems().Count();

            if (changedItemsCount != contextChangedItemsCount)
            {
                throw new UnexpectedChangesException();
            }
        }

        private void AssertExactlyChanged<T>(T[] items) where T: class
        {
            var changedItemsCount = items
                .Distinct()
                .Select(i => _dataContext.Entry(i))
                .Where(e => e.State != EntityState.Detached && e.State != EntityState.Unchanged)
                .Count();
            var contextChangedItemsCount = GetChangeItems().Count();

            if (changedItemsCount != contextChangedItemsCount)
            {
                throw new UnexpectedChangesException();
            }
        }

        private IEnumerable<EntityEntry> GetChangeItems()
        {
            return _dataContext.ChangeTracker.Entries()
                .Where(e => e.State != EntityState.Detached && e.State != EntityState.Unchanged);
        }

        private void AssertAttached(object item)
        {
            var entityEntry = _dataContext.Entry(item);
            if (entityEntry == null || entityEntry.State == EntityState.Detached)
            {
                throw new ItemDetachedException(item);
            }
        }

        private void AssertAttached(object[] items)
        {
            foreach (var item in items)
            {
                AssertAttached(item);
            }
        }

        private void AssertAttached<T>(T item) where T: class
        {
            var entityEntry = _dataContext.Entry(item);
            if (entityEntry == null || entityEntry.State == EntityState.Detached)
            {
                throw new ItemDetachedException(item);
            }
        }

        private void AssertAttached<T>(T[] items) where T: class
        {
            foreach (var item in items)
            {
                AssertAttached(item);
            }
        }

        private static void AssertIsClass(Type type)
        {
            if (!type.IsClass)
            {
                throw new InvalidOperationException(string.Format("{0} is not class.", type.FullName));
            }
        }

        private void SafeCall(Action action)
        {
            try
            {
                action();
            }
            catch (DbUpdateConcurrencyException e)
            {
                _logger.LogWarning(GetErrorMessage(), e);
                throw new ConcurrentCallException(e);
            }
            catch (Exception e)
            {
                _logger.LogWarning(GetErrorMessage(), e);
                throw new DbException("DB call is failed", e);
            }
        }

        private Task SafeCallAsync(Func<Task> action)
        {
            try
            {
                return action();
            }
            catch (DbUpdateConcurrencyException e)
            {
                _logger.LogWarning(GetErrorMessage(), e);
                throw new ConcurrentCallException(e);
            }
            catch (Exception e)
            {
                _logger.LogWarning(GetErrorMessage(), e);
                throw new DbException("DB call is failed", e);
            }
        }

        private static string GetErrorMessage()
        {
            return "Ошибка при обращении к БД";
        }
    }
}
