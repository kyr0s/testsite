﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NewsService.DAL.IoC;

namespace NewsService.Impl.IoC
{
    public static class Module
    {
        public static IServiceCollection WithNewsService(this IServiceCollection @this, DbContextOptions options)
        {
            return @this
                .WithNewsDataLayer(options)
                .AddScoped<INewsService, NewsService>();
        }
    }
}
