﻿using System;
using System.Threading.Tasks;
using NewsService.DAL;
using NewsService.Models;

namespace NewsService.Impl
{
    internal class NewsService : INewsService
    {
        private readonly INewsRepository _repository;

        public NewsService(INewsRepository repository)
        {
            _repository = repository;
        }

        public async Task<Guid> CreateAsync(CreateNewsRequest request)
        {
            var news = new News
            {
                Id = Guid.NewGuid(),
                Date = DateTime.Now,
                Title = request.Title,
                Text = request.Text
            };

            await _repository.AddAsync(news).ConfigureAwait(false);
            return news.Id;
        }

        public Task<NewsPreview[]> GetPageAsync(int pageNumber, int pageSize)
        {
            return _repository.ListAsync(pageNumber, pageSize);
        }

        public Task<News> GetAsync(Guid newsId)
        {
            return _repository.GetAsync(newsId);
        }

        public Task<Comment[]> SelectCommentsAsync(Guid newsId)
        {
            return _repository.SelectCommentsAsync(newsId);
        }

        public async Task<Guid> CreateCommentAsync(Guid newsId, Guid userId, string text)
        {
            var comment = new Comment
            {
                Id = Guid.NewGuid(),
                CreateDate = DateTime.Now,
                UserId = userId,
                Text = text
            };
            await _repository.CreateCommentAsync(newsId, comment).ConfigureAwait(false);
            return comment.Id;
        }
    }
}
