﻿using Common.DAL.EF.IoC;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace NewsService.DAL.IoC
{
    public static class Module
    {
        public static IServiceCollection WithNewsDataLayer(this IServiceCollection @this, DbContextOptions options)
        {
            return @this
                .WithEfDbContext<NewsZone>(() => new NewsContext(options))
                .AddScoped<INewsRepository, NewsRepository>();
        }
    }
}
