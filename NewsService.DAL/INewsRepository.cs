﻿using System;
using System.Threading.Tasks;
using NewsService.Models;

namespace NewsService.DAL
{
    public interface INewsRepository
    {
        Task<NewsPreview[]> ListAsync(int pageNumber, int pageSize);
        Task<News> GetAsync(Guid id);
        Task AddAsync(News news);
        Task<Comment[]> SelectCommentsAsync(Guid newsId);
        Task CreateCommentAsync(Guid newsId, Comment comment);
    }
}
