﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.DAL.Attributes;

namespace NewsService.DAL.Entities
{
    [Schema("news")]
    internal class DbNews
    {
        public Guid Id { get; set; }

        [Column(TypeName = "date")]
        public DateTime Date { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(50)]
        public string Title { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(50)]
        public string Text { get; set; }

        public virtual ICollection<DbComment> Comments { get; set; }
    }
}
