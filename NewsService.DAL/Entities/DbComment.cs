﻿using System;
using System.ComponentModel.DataAnnotations;
using Common.DAL.Attributes;

namespace NewsService.DAL.Entities
{
    [Schema("news")]
    internal class DbComment
    {
        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        public Guid NewsId { get; set; }

        [Required]
        [MaxLength(4000)]
        public string Text { get; set; }

        public DateTime CreateDate { get; set; }
    }
}
