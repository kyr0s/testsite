﻿using Common.DAL.EF.Ext;
using Microsoft.EntityFrameworkCore;
using NewsService.DAL.Entities;

namespace NewsService.DAL
{
    internal class NewsContext : DbContext
    {
        public NewsContext(DbContextOptions options) : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DbComment>()
                .HasOne(typeof(DbNews))
                .WithMany(nameof(DbNews.Comments))
                .HasForeignKey(nameof(DbComment.NewsId))
                .HasPrincipalKey(nameof(DbNews.Id));

            modelBuilder.SetUnderscoreSnakeConventions();
        }

        public DbSet<DbNews> News { get; set; }
        public DbSet<DbComment> Comments { get; set; }
    }
}
