﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Common.DAL;
using Microsoft.EntityFrameworkCore;
using NewsService.DAL.Entities;
using NewsService.DAL.IoC;
using NewsService.Models;

namespace NewsService.DAL
{
    internal class NewsRepository : INewsRepository
    {
        private readonly IDbContextProvider<NewsZone> _provider;
        private IDataContext<NewsZone> Context => _provider.Get();

        public NewsRepository(IDbContextProvider<NewsZone> provider)
        {
            _provider = provider;
        }

        public async Task<NewsPreview[]> ListAsync(int pageNumber, int pageSize)
        {
            var skipCount = (pageNumber - 1) * pageSize;

            var dbNews = await Context
                    .Query<DbNews>()
                    .OrderBy(n => n.Date)
                    .ThenBy(n => n.Id)
                    .Skip(skipCount)
                    .Take(pageSize)
                    .ToArrayAsync()
                    .ConfigureAwait(false);

            return dbNews
                .Select(n => new NewsPreview {Id = n.Id, Title = n.Title, Date = n.Date})
                .ToArray();
        }

        public async Task<News> GetAsync(Guid id)
        {
            var found = await Context
                .Query<DbNews>()
                .FirstOrDefaultAsync(n => n.Id == id)
                .ConfigureAwait(false);

            if (found == null)
            {
                throw new Exception($"News with id={id} not found!");
            }

            var news = new News
            {
                Id = found.Id,
                Date = found.Date,
                Title = found.Title,
                Text = found.Text
            };
            return news;
        }

        public Task AddAsync(News news)
        {
            var dbNews = new DbNews
            {
                Id = news.Id,
                Date = news.Date,
                Title = news.Title,
                Text = news.Text
            };

            return Context.CreateAsync(dbNews);
        }

        public Task CreateCommentAsync(Guid newsId, Comment comment)
        {
            var dbComment = Convert(comment, newsId);
            return Context.CreateAsync(dbComment);
        }

        public async Task<Comment[]> SelectCommentsAsync(Guid newsId)
        {
            var dbComments = await Context.Query<DbComment>()
                .Where(c => c.NewsId == newsId)
                .ToArrayAsync()
                .ConfigureAwait(false);

            var comments = dbComments.Select(Convert).ToArray();
            return comments;
        }

        private Comment Convert(DbComment comment)
        {
            return new Comment
            {
                Id = comment.Id,
                CreateDate = comment.CreateDate,
                Text = comment.Text,
                UserId = comment.UserId
            };
        }

        private DbComment Convert(Comment comment, Guid newsId)
        {
            return new DbComment
            {
                Id = comment.Id,
                NewsId = newsId,
                UserId = comment.UserId,
                CreateDate = comment.CreateDate,
                Text = comment.Text
            };
        }
    }
}