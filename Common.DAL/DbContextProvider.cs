﻿using System;
using System.Threading;
using Common.DAL.Contexts;
using Microsoft.Extensions.DependencyInjection;

namespace Common.DAL
{
    internal class DbContextProvider<TZone>: IDbContextProvider<TZone> where TZone: IZoneMarker
    {
        private readonly Lazy<IDataContext<TZone>> _global;
        private readonly IServiceProvider _serviceProvider;

        public DbContextProvider(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            _global = new Lazy<IDataContext<TZone>>(CreateContext, LazyThreadSafetyMode.ExecutionAndPublication);
        }

        public IDataContext<TZone> Get()
        {
            if (!QueryContext.HasScope())
            {
                return _global.Value;
            }

            var dbContext = QueryContext.GetOrCreate(CreateContext);
            return dbContext;
        }

        private IDataContext<TZone> CreateContext()
        {
            return _serviceProvider.GetRequiredService<IDataContext<TZone>>();
        }
    }
}