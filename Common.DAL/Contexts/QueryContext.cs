﻿using System;
using System.Collections.Concurrent;

namespace Common.DAL.Contexts
{
    public static class QueryContext
    {
        private const string _key = "QueryContext_90a4991e-e2c5-4683-9d87-352aef8e961d";
        private static readonly ConcurrentDictionary<Guid, IDisposable> _scopeData = new ConcurrentDictionary<Guid, IDisposable>();

        public static IDisposable CreateScope()
        {
            var parentScope = CallContext<QueryScope>.GetData(_key);
            var scope = new QueryScope(parentScope, OnDispose);
            CallContext<QueryScope>.SetData(_key, scope);
            return scope;
        }

        public static bool HasScope()
        {
            var scope = CallContext<QueryScope>.GetData(_key);
            return scope != null;
        }

        public static T GetOrCreate<T>(Func<T> valueFactory) where T: IDisposable
        {
            var scope = CallContext<QueryScope>.GetData(_key);
            if (scope == null)
            {
                throw new InvalidOperationException("Scope does not exist");
            }

            var data = (T)_scopeData.GetOrAdd(scope.Id, _ => valueFactory());
            return data;
        }

        private static void OnDispose(QueryScope scope)
        {
            if (_scopeData.TryRemove(scope.Id, out var data))
            {
                data.Dispose();
                CallContext<QueryScope>.SetData(_key, scope.Parent);
            }
        }

        private class QueryScope : IDisposable
        {
            private readonly Action<QueryScope> _disposeAction;

            public QueryScope(QueryScope parent, Action<QueryScope> disposeAction)
            {
                Id = Guid.NewGuid();
                Parent = parent;
                _disposeAction = disposeAction;
            }

            public Guid Id { get; }

            public QueryScope Parent { get; }

            public void Dispose()
            {
                _disposeAction(this);
            }
        }
    }
}
