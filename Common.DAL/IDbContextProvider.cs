﻿namespace Common.DAL
{
    public interface IDbContextProvider<T> where T: IZoneMarker
    {
        IDataContext<T> Get();
    }
}