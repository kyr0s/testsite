﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Common.DAL.Attributes
{
    public class SchemaAttribute : TableAttribute
    {
        public const string MockTableName = "1e0ddcf3a5564aa4a441efe39790082b";

        public SchemaAttribute(string schema) : base(MockTableName)
        {
            Schema = schema;
        }
    }
}
