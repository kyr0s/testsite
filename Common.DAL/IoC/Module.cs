﻿using Microsoft.Extensions.DependencyInjection;

namespace Common.DAL.IoC
{
    public static class Module
    {
        public static IServiceCollection WithDataLayer<TZone>(this IServiceCollection @this) where TZone: IZoneMarker
        {
            return @this
                .AddScoped<IDbContextProvider<TZone>, DbContextProvider<TZone>>();
        }
    }
}
