﻿namespace Common.DAL.Exceptions
{
    public class ItemDetachedException : DbException
    {
        public ItemDetachedException(object item) : base($"Item {item} detached from current context")
        {
        }
    }
}