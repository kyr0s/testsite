﻿using System;

namespace Common.DAL.Exceptions
{
    public class ConcurrentCallException : DbException
    {
        public ConcurrentCallException(Exception e)
            : base("Concurrent call!", e)
        {
        }
    }
}