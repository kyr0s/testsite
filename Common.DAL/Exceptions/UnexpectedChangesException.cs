﻿namespace Common.DAL.Exceptions
{
    public class UnexpectedChangesException : DbException
    {
        public UnexpectedChangesException() : base("Unexpected items changed!")
        {
        }
    }
}