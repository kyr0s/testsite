﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace Common.DAL
{
    public interface IDataContext : IDisposable
    {
        IQueryable<T> Query<T>() where T : class;

        void Create(Type type, object item);
        Task CreateAsync(Type type, object item);
        void Create<T>(T item) where T : class;
        Task CreateAsync<T>(T item) where T : class;
        void CreateAll(Type type, object[] items);
        Task CreateAllAsync(Type type, object[] items);
        void CreateAll<T>(T[] items) where T : class;
        Task CreateAllAsync<T>(T[] items) where T : class;

        void Update(Type type, object item);
        Task UpdateAsync(Type type, object item);
        void Update<T>(T item) where T: class;
        Task UpdateAsync<T>(T item) where T: class;
        void UpdateAll(Type type, object[] items);
        Task UpdateAllAsync(Type type, object[] items);
        void UpdateAll<T>(T[] items) where T : class;
        Task UpdateAllAsync<T>(T[] items) where T : class;

        void Delete(Type type, object item);
        Task DeleteAsync(Type type, object item);
        void Delete<T>(T item) where T : class;
        Task DeleteAsync<T>(T item) where T : class;
        void DeleteAll(Type type, object[] items);
        Task DeleteAllAsync(Type type, object[] items);
        void DeleteAll<T>(T[] items) where T : class;
        Task DeleteAllAsync<T>(T[] items) where T : class;
    }

    public interface IDataContext<TZone> : IDataContext where TZone : IZoneMarker
    {
    }
}
