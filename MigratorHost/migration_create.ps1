function AssertExitCodeSuccessful( $message )
{
    if ($LastExitCode -ne 0)
    {
        Write-Host "Unexpected exit code $LastExitCode"
        if ($message)
        {
            Write-Host $message
        }

        Write-Error "Error!!!"
        Exit
    }
}

dotnet build
AssertExitCodeSuccessful "Build failed!"

$dbContexts = (dotnet ef dbcontext list --no-build)
AssertExitCodeSuccessful "Contexts list failed!"

$gitBranch = (git rev-parse --abbrev-ref HEAD)
AssertExitCodeSuccessful "Get current git branch failed!"

Foreach ($dbContext in $dbContexts)
{
    $migrationId = [System.Guid]::NewGuid().ToString("N")
    $migrationNameRaw = "$($gitBranch)_$migrationId"
    $migrationName = $migrationNameRaw -replace "[^\d\w]", "_"

    $tempScriptPath = "$migrationName.tmp.sql"

    dotnet ef migrations add $migrationName --context $dbContext --no-build
    AssertExitCodeSuccessful "Can't add migration for context $dbContext"

    dotnet ef migrations script --context $dbContext --idempotent --output $tempScriptPath
    AssertExitCodeSuccessful "Can't generate script for context $dbContext"

    Add-Content ConvertDb.sql "`n-- <$migrationNameRaw> $env:UserName`n"
    Get-Content $tempScriptPath | Add-Content ConvertDb.sql
    Add-Content ConvertDb.sql "`n-- </$migrationNameRaw>`n"

    Remove-Item $tempScriptPath -Force
    Get-ChildItem "Migrations" -File -Exclude "*ModelSnapshot.cs" -Recurse | Remove-Item
}