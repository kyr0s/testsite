function AssertExitCodeSuccessful( $message )
{
    if ($LastExitCode -ne 0)
    {
        Write-Host "Unexpected exit code $LastExitCode"
        if ($message)
        {
            Write-Host $message
        }

        Write-Error "Error!!!"
        Exit
    }
}

$tag = (git describe origin/master --match="v*")
AssertExitCodeSuccessful "Command 'git describe origin/master --match=`"v*`"' failed"

New-Item -ItemType Directory -Force -Path "Archives"

$archivePath = "Archives\$tag.sql"
if (Test-Path $archivePath)
{
    Write-Host "Archive script for version $tag already exists.`nMaybe you forgot add current version git tag?"
    Write-Error ""
    Exit
}

Get-Content "ConvertDb.sql" | Add-Content $archivePath

Remove-Item "ConvertDb.sql"
New-Item -Path . -Name "ConvertDb.sql" -ItemType File
Get-Content "ConvertDb-next.sql" | Add-Content "ConvertDb.sql"

Remove-Item ConvertDb-next.sql
New-Item -Path . -Name "ConvertDb-next.sql" -ItemType File
