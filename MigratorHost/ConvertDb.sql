﻿
-- <feature/user_name_caf290b1354349a18f0f63cfc7f80185-next> chicherskiy

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20170922074214_feature_user_name_caf290b1354349a18f0f63cfc7f80185-next') THEN
    UPDATE "users"."user" SET "name" = "login" WHERE "name" IS NULL;
    ALTER TABLE "users"."user" ALTER COLUMN "name" varchar(50) NOT NULL;
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20170922074214_feature_user_name_caf290b1354349a18f0f63cfc7f80185-next') THEN
    INSERT INTO "__EFMigrationsHistory" ("MigrationId", "ProductVersion")
    VALUES ('20170922074214_feature_user_name_caf290b1354349a18f0f63cfc7f80185-next', '2.0.0-rtm-26452');
    END IF;
END $$;

-- </feature/user_name_caf290b1354349a18f0f63cfc7f80185-next>

