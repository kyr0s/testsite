﻿
-- <master_98935f68481543ea86374f639b267c22> chicherskiy

CREATE TABLE IF NOT EXISTS "__EFMigrationsHistory" (
    "MigrationId" varchar(150) NOT NULL,
    "ProductVersion" varchar(32) NOT NULL,
    CONSTRAINT "PK___EFMigrationsHistory" PRIMARY KEY ("MigrationId")
);


DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20170921131759_master_98935f68481543ea86374f639b267c22') THEN
    CREATE SCHEMA IF NOT EXISTS "news";
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20170921131759_master_98935f68481543ea86374f639b267c22') THEN
    CREATE TABLE "news"."news" (
        "id" uuid NOT NULL,
        "date" date NOT NULL,
        "text" varchar(50) NOT NULL,
        "title" varchar(50) NOT NULL,
        CONSTRAINT "pk_news" PRIMARY KEY ("id")
    );
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20170921131759_master_98935f68481543ea86374f639b267c22') THEN
    INSERT INTO "__EFMigrationsHistory" ("MigrationId", "ProductVersion")
    VALUES ('20170921131759_master_98935f68481543ea86374f639b267c22', '2.0.0-rtm-26452');
    END IF;
END $$;
-- </master_98935f68481543ea86374f639b267c22>


-- <master_35db690e6b7e4697871f9d6b4f9b20ca> chicherskiy

CREATE TABLE IF NOT EXISTS "__EFMigrationsHistory" (
    "MigrationId" varchar(150) NOT NULL,
    "ProductVersion" varchar(32) NOT NULL,
    CONSTRAINT "PK___EFMigrationsHistory" PRIMARY KEY ("MigrationId")
);


DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20170921131816_master_35db690e6b7e4697871f9d6b4f9b20ca') THEN
    CREATE SCHEMA IF NOT EXISTS "users";
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20170921131816_master_35db690e6b7e4697871f9d6b4f9b20ca') THEN
    CREATE TABLE "users"."user" (
        "id" uuid NOT NULL,
        "create_date" date NOT NULL,
        "email" varchar(50) NOT NULL,
        "login" varchar(50) NOT NULL,
        "password" varchar(64) NOT NULL,
        CONSTRAINT "pk_user" PRIMARY KEY ("id")
    );
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20170921131816_master_35db690e6b7e4697871f9d6b4f9b20ca') THEN
    INSERT INTO "__EFMigrationsHistory" ("MigrationId", "ProductVersion")
    VALUES ('20170921131816_master_35db690e6b7e4697871f9d6b4f9b20ca', '2.0.0-rtm-26452');
    END IF;
END $$;
-- </master_35db690e6b7e4697871f9d6b4f9b20ca>

