﻿
-- <feature/comments_a2348b98b0dc4fa9bbafdabfd456c09b> chicherskiy

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20170922082211_feature_comments_a2348b98b0dc4fa9bbafdabfd456c09b') THEN
    CREATE TABLE "news"."comment" (
        "id" uuid NOT NULL,
        "create_date" timestamp NOT NULL,
        "news_id" uuid NOT NULL,
        "text" varchar(4000) NOT NULL,
        "user_id" uuid NOT NULL,
        CONSTRAINT "pk_comment" PRIMARY KEY ("id"),
        CONSTRAINT "fk_comment_comment_news_id" FOREIGN KEY ("news_id") REFERENCES "news"."news" ("id") ON DELETE CASCADE
    );
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20170922082211_feature_comments_a2348b98b0dc4fa9bbafdabfd456c09b') THEN
    CREATE INDEX "ix_comment_news_id" ON "news"."comment" ("news_id");
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20170922082211_feature_comments_a2348b98b0dc4fa9bbafdabfd456c09b') THEN
    INSERT INTO "__EFMigrationsHistory" ("MigrationId", "ProductVersion")
    VALUES ('20170922082211_feature_comments_a2348b98b0dc4fa9bbafdabfd456c09b', '2.0.0-rtm-26452');
    END IF;
END $$;

-- </feature/comments_a2348b98b0dc4fa9bbafdabfd456c09b>

-- <feature/user_name_caf290b1354349a18f0f63cfc7f80185> chicherskiy

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20170922074214_feature_user_name_caf290b1354349a18f0f63cfc7f80185') THEN
    ALTER TABLE "users"."user" ADD "name" varchar(50) NULL;
    UPDATE "users"."user" SET "name" = "login";
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "__EFMigrationsHistory" WHERE "MigrationId" = '20170922074214_feature_user_name_caf290b1354349a18f0f63cfc7f80185') THEN
    INSERT INTO "__EFMigrationsHistory" ("MigrationId", "ProductVersion")
    VALUES ('20170922074214_feature_user_name_caf290b1354349a18f0f63cfc7f80185', '2.0.0-rtm-26452');
    END IF;
END $$;

-- </feature/user_name_caf290b1354349a18f0f63cfc7f80185>

