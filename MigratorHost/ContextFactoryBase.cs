﻿using System;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace MigratorHost
{
    abstract class ContextFactoryBase<T> : IDesignTimeDbContextFactory<T> where T: DbContext
    {
        public T CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder();
            var assemblyName = Assembly.GetExecutingAssembly().FullName;
            optionsBuilder.UseNpgsql("Server=192.168.0.236; Database=some_db; User Id=megauser; Password=secret!;", b => b.MigrationsAssembly(assemblyName));
            return (T)Activator.CreateInstance(typeof(T), optionsBuilder.Options);
        }
    }
}