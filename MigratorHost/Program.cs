﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NewsService.DAL;
using NewsService.DAL.IoC;

namespace MigratorHost
{
    class Program
    {
        static void Main(string[] args)
        {
            var servicesCollection = new ServiceCollection();
            var options = CreateDbContextOptions();
            servicesCollection.WithNewsDataLayer(options);
            var provider = servicesCollection.BuildServiceProvider();

            var repository = provider.CreateScope().ServiceProvider.GetRequiredService<INewsRepository>();
            Console.Write($"{repository} ^_^ ");
            Console.ReadKey();
        }

        private static DbContextOptions CreateDbContextOptions()
        {
            var optionsBuilder = new DbContextOptionsBuilder();
            optionsBuilder.UseNpgsql("Server=192.168.0.236; Database=pcc_dsfdsfsdfsfsfsdf; User Id=pccuser; Password=q1w2e3R$;");
            return optionsBuilder.Options;
        }
    }
}
