﻿using Common.DAL.EF.IoC;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace UserService.DAL.IoC
{
    public static class Module
    {
        public static IServiceCollection WithUsersDataLayer(this IServiceCollection @this, DbContextOptions options)
        {
            return @this
                .WithEfDbContext<UsersZone>(() => new UsersContext(options))
                .AddScoped<IUserRepository, UserRepository>();
        }
    }
}
