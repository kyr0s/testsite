﻿using Common.DAL.EF.Ext;
using Microsoft.EntityFrameworkCore;
using UserService.DAL.Entities;

namespace UserService.DAL
{
    internal class UsersContext : DbContext
    {
        public UsersContext(DbContextOptions options) : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.SetUnderscoreSnakeConventions();
        }

        public DbSet<DbUser> Users { get; set; }
    }
}
