﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Common.DAL.Attributes;

namespace UserService.DAL.Entities
{
    [Schema("users")]
    internal class DbUser
    {
        public Guid Id { get; set; }

        [Column(TypeName = "date")]
        public DateTime CreateDate { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(50)]
        public string Login { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(50)]
        public string Email { get; set; }

        [Required]
        [MinLength(64)]
        [MaxLength(64)]
        public string Password { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(50)]
        public string Name { get; set; }
    }
}
