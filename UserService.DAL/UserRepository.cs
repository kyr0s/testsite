﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Common.DAL;
using Microsoft.EntityFrameworkCore;
using UserService.DAL.Entities;
using UserService.DAL.IoC;
using UserService.Models;

namespace UserService.DAL
{
    internal class UserRepository : IUserRepository
    {
        private readonly IDbContextProvider<UsersZone> _provider;
        private IDataContext<UsersZone> Context => _provider.Get();

        public UserRepository(IDbContextProvider<UsersZone> provider)
        {
            _provider = provider;
        }

        public async Task SaveAsync(User user)
        {
            var dbUser = await Context.Query<DbUser>()
                .Where(u => u.Id == user.Id)
                .FirstOrDefaultAsync()
                .ConfigureAwait(false);

            if (dbUser == null)
            {
                await CreateAsync(user).ConfigureAwait(false);
            }
            else
            {
                await UpdateAsync(dbUser, user).ConfigureAwait(false);
            }
        }

        private Task CreateAsync(User user)
        {
            var dbUser = new DbUser
            {
                Id = user.Id,
                CreateDate = user.CreateDate,
                Login = user.Login,
                Email = user.Email,
                Password = user.Password,
                Name = user.Name
            };

            return Context.CreateAsync(dbUser);
        }

        private Task UpdateAsync(DbUser dbUser, User user)
        {
            dbUser.Email = user.Email;
            dbUser.Login = user.Login;
            dbUser.Password = user.Password;
            dbUser.Name = user.Name;

            return Context.UpdateAsync(dbUser);
        }

        public async Task<User> AuthenticateAsync(string login, string password)
        {
            var dbUser = await Context.Query<DbUser>()
                .Where(u => u.Login == login && u.Password == password)
                .FirstOrDefaultAsync()
                .ConfigureAwait(false);

            return Convert(dbUser);
        }

        public async Task<User> GetAsync(Guid id)
        {
            var dbUser = await Context.Query<DbUser>()
                .Where(u => u.Id == id)
                .FirstOrDefaultAsync()
                .ConfigureAwait(false);

            if (dbUser == null)
            {
                throw new Exception($"User with id={id} not found");
            }

            return Convert(dbUser);
        }

        private User Convert(DbUser dbUser)
        {
            if (dbUser == null)
            {
                return null;
            }

            return new User
            {
                Id = dbUser.Id,
                Login = dbUser.Login,
                Email = dbUser.Email,
                CreateDate = dbUser.CreateDate,
                Password = dbUser.Password,
                Name = dbUser.Name
            };
        }
    }
}