﻿using System;
using System.Threading.Tasks;
using UserService.Models;

namespace UserService.DAL
{
    public interface IUserRepository
    {
        Task SaveAsync(User user);
        Task<User> AuthenticateAsync(string login, string password);
        Task<User> GetAsync(Guid id);
    }
}
